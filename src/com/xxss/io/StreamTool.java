package com.xxss.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.util.Log;

public class StreamTool {
	public static byte[] readInputStream(InputStream inStream) throws IOException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int len = 0;
		 
		while ((len = inStream.read(buffer)) != -1) { 
			outStream.write(buffer, 0, len);
		} 
		inStream.close();
		return outStream.toByteArray();
	}
	
	
	public static byte[] getData(String url){
		Log.d("StreamTool.getDate","from url "+url);
		 try {
			URL u = new URL(url);
			HttpURLConnection conn=(HttpURLConnection)u.openConnection();
			conn.setConnectTimeout(200);
			conn.setRequestMethod("GET");
			InputStream in=conn.getInputStream();
			
			byte[] bts=readInputStream(in);
			return bts;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return null;
		 

	}
}
