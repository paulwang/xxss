package com.xxss.io;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

class BitmapWorkerTask2 extends AsyncTask<Integer,BitmapWorkerTask2.LoadedBitmap, Bitmap> {
    private WeakReference<ImageView>[] imageViewReferences;
    private Map<String,WeakReference<ImageView>> urlAndImages=new HashMap<String,WeakReference<ImageView>>();
    public class LoadedBitmap{
    	protected String url;
    	protected Bitmap bitmap;
    	public LoadedBitmap(String url,Bitmap bitmap){
    		this.url=url;
    		this.bitmap=bitmap;
    	}
    }

    public BitmapWorkerTask2(Map<String,ImageView> urlAndImages) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
    	for(String url:urlAndImages.keySet()){
    		this.urlAndImages.put(url, new WeakReference<ImageView>(urlAndImages.get(url)));
    	}
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... num) {
    	for(String url : this.urlAndImages.keySet()){
    		byte[] data=StreamTool.getData(url);
        	Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        	this.publishProgress(new LoadedBitmap(url,bitmap));
    	}
        return null;/// decodeSampledBitmapFromResource(null, data, 100, 100));
    }
    
    @Override
	protected void onProgressUpdate(LoadedBitmap... lbs) {
    	LoadedBitmap loadedBitmap=lbs[0];
    	WeakReference<ImageView> imageViewReference=this.urlAndImages.get(loadedBitmap.url);
    	Bitmap bitmap=loadedBitmap.bitmap;
		if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
		}
	}

	// Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
//        if (imageViewReference != null && bitmap != null) {
//            final ImageView imageView = imageViewReference.get();
//            if (imageView != null) {
//                imageView.setImageBitmap(bitmap);
//            }
//        }
    }
}