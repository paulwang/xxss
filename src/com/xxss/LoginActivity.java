package com.xxss;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.util.Log;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}
	public void doLogin(View view){
		TextView inputUserName=(TextView)findViewById(R.id.userName);
		TextView inputPassword=(TextView)findViewById(R.id.password);
		String userName=inputUserName.getText().toString();
		String password=inputPassword.getText().toString();
		Log.d("UserName:",userName);
		Log.d("Password:",password);
	}
}
