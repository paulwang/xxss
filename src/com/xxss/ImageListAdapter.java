package com.xxss;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.xxss.io.BitmapWorkerTask;

/**
 * 图片列表的适配器
 * 
 * @author Paul Wang <wagzhi@gmail.com>
 * @since 2011-11-26
 */
public class ImageListAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private Context context;
	private List<String> items;

	public ImageListAdapter(Context context, List<String> items) {
		this.context = (MainActivity) context;
		this.mInflater = LayoutInflater.from(context);
		if (items == null) {
			this.items = new ArrayList<String>();
			for (int i = 0; i < 5; i++) {
				this.items.add("");
			}
		} else
			this.items = items;
	}

	@Override
	public int getCount() {
		return this.items.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.d("ListView Adapter Monitor: ", "getView " + position);
		if (convertView == null) {
			convertView = this.mInflater.inflate(R.layout.images, null);

			ImageView imgView = (ImageView) convertView
					.findViewById(R.id.imageView1);
			String url = "http://att3.citysbs.com/780x/haodian/2013/11/14/21/middle_1000x1499-214010_17531384436410092_05c0fa9c0c03eec87f249233ce7d199b.jpg";
			BitmapWorkerTask task = new BitmapWorkerTask(imgView);
			task.execute(url);
		}
		return convertView;
	}

	private String getFileName(String fullName) {
		String[] part = fullName.split("/");
		return part[part.length - 1];
	}

	public void showInfo() {
		new AlertDialog.Builder(context).setTitle("我的listview")
				.setMessage("介绍...")
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				}).show();

	}

	/**
	 * 获取网落图片资源
	 * 
	 * @param url
	 * @return
	 */
	public static Bitmap getHttpBitmap(String url) {
		URL myFileURL;
		Bitmap bitmap = null;
		try {
			myFileURL = new URL(url);
			// 获得连接
			HttpURLConnection conn = (HttpURLConnection) myFileURL
					.openConnection();
			// 设置超时时间为6000毫秒，conn.setConnectionTiem(0);表示没有时间限制
			conn.setConnectTimeout(6000);
			// 连接设置获得数据流
			conn.setDoInput(true);
			// 不使用缓存
			conn.setUseCaches(false);
			// 这句可有可无，没有影响
			// conn.connect();
			// 得到数据流
			InputStream is = conn.getInputStream();
			// 解析得到图片
			bitmap = BitmapFactory.decodeStream(is);
			// 关闭数据流
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return bitmap;

	}

}
