package com.xxss;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//ActionBar actionBar = this.getActionBar();
		//actionBar.hide();
		
		GridView gv=(GridView)findViewById(R.id.gridView);
		//ArrayAdapter<String> ad=new ArrayAdapter<String>(this,R.layout.images,new String[]{"1","2","3","4","5"});
		ArrayList<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
		for(int i=0;i<5;i++){
			HashMap<String,Object> item = new HashMap<String,Object>();
			item.put("img", R.drawable.default_img);
			list.add(item);
		}
		//SimpleAdapter  adapter = new SimpleAdapter( this,list,R.layout.images,new String[]{"img"},new int[]{R.id.imageView1});
		ImageListAdapter adapter= new ImageListAdapter(this,null);
		gv.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		 switch (item.getItemId()) {
		 case R.id.action_login:
			 showLogin();
			 return true;
		 }
		return super.onOptionsItemSelected(item);
	}
	
	private void showLogin(){
		Intent intent=new Intent(this,LoginActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.in_from_right,R.anim.out_to_left);
		
	}

}
